import React, { useState } from 'react';
import './App.css';
import { DB_SECRETS, generateInsertStatement } from './consts';

const App: React.FC = () => {

  const [sqlStatement, setSqlStatement] = useState('Place stuff in inputs and generate the mysql');
  return (
    <div className="App">
      <h1>IZ Manual Provisioning Helper</h1>
      <div className='left'>
        <div className='iz-input'>
          <h3>Run this in common tenant database to get the next ID to use:</h3>
          <pre>SELECT nextval('common.sq_asset_meta_id');</pre>
          <h4>Common Tenant Id</h4>
          <input id='CommonTenantId' type='text' />
        </div>
        <div className='iz-input'>
          <h4>Informz Brand Id</h4>
          <input id='BrandId' type='text' />
        </div>
        <div className='iz-input'>
          <h4>Desired HigherLogicWeb Subdomain (Subdomain Only!!!)</h4>
          <p>When logged in, this will be the subdomain in <i><b>subdomain</b></i>.higherlogic.com</p>
          <input id='HigherLogicWebSubdomain' type='text' />
        </div>
        <div className='iz-input'>
          <h4>IZ Login Domain Name</h4>
          <p>Fully qualified domain where you log in. Note that there is the corresponding <i>brand_sub_domain</i> field in the iz <i>brand</i> record. It should match this value.</p>
          <input id='IzLoginDomain' type='text' />
        </div>
        <div className='iz-input'>
          <h4>IZ Environment</h4>
          <p>This hooks up the proper dbSecret Key</p>
          <select id='DbSecretKey'>
            <option value={DB_SECRETS.DEV3}>Dev3</option>
            <option value={DB_SECRETS.DEV2}>Dev2</option>
            <option value={DB_SECRETS.RC}>RC</option>

          </select>
        </div>
        <div className='iz-input'>
          <button className='generate-button' onClick={() => {
            setSqlStatement(generateInsertStatement())
          }}>Generate Query For Manual Provision</button>
        </div>
      </div>
      <div className='right'>
        <h4>Final Query</h4>
        <textarea id='ManualProvisionQuery' value={sqlStatement}/>
      </div>

    </div>
  );
}

export default App;
