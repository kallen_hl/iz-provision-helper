import generate from '@babel/generator';

export const DB_SECRETS = {
  DEV2: 'dev2db',
  DEV3: 'dev3Sql01',
  RC: 'rcpod1'
};

export const generateHigherLogicWebDomain = '';
/**
 * Note that pkey and label are identical
 */
export const generateCommonTenantInserts = (
  desiredId: number,
  desiredHigherLogicWebSubDomain: string,
  loginPageDomain: string,
  dbSecretKey: string,
  pkey: string,
  label: string
) => `
-- note that pkey and label are identical
DECLARE @commonTenantId INT;

select @commonTenantId = nextval('common.sq_asset_meta_id');

INSERT INTO common.asset(id, parent_id, type_id, origin_id, pkey, label, s1, s2, s3, s4, s5, ext, modified, mod_id)
  VALUES (${desiredId},0,99,0,${pkey},'${label}',NULL,'${desiredHigherLogicWebSubDomain}.higherlogic.com',NULL,NULL,NULL,'{}',now(),0);
INSERT INTO common.asset(id, parent_id, type_id, origin_id, pkey, label, s1, s2, s3, s4, s5, ext, modified, mod_id)
  VALUES (${desiredId},0,102,3,${pkey},'${label}',${label},'Sync Ready',NULL,NULL,NULL,'{"dbName": "Informz31", "domain": "${loginPageDomain}", "dbSecretKey": "${dbSecretKey}", "adminCoreSynced": ""}',now(),0);
`;

export const generateInsertStatement = () => {
  const getValue = (id: string) => (document.getElementById(id) as any).value;
  const hlSubdomain = getValue('HigherLogicWebSubdomain');
  const loginPageDomain = getValue('IzLoginDomain');
  const dbSecretKey = getValue('DbSecretKey');
  const pkeyLabel = getValue('BrandId');
  const id = getValue('CommonTenantId');

  return generateCommonTenantInserts(
    id,
    hlSubdomain,
    loginPageDomain,
    dbSecretKey,
    pkeyLabel,
    pkeyLabel
  );
};
